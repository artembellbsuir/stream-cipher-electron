let keys, registers, powers, maxPowers, maxs, masks, poppedBits = [], geffeKey = 0

export const initGeffeRegisters = (initialStates, allPowers) => {
    registers = initialStates
    powers = allPowers
    keys = [0, 0, 0]
    maxPowers = powers.map(powerArr => Math.max(...powerArr))
    maxs = maxPowers.map(power => 2 ** power)
    masks = powers.map((powerArr, i) => powerArr.map(power => 2 ** (maxPowers[i] - power)))
}

const calcNextKey = (k) => {

    const xors =  masks[k].map(int => (registers[k] & int) === int ? 1 : 0)
    const bit = xors.reduce((prev, next) => prev ^ next)

    poppedBits[k] = registers[k] & 1;

    // just shift i-ish key
    keys[k] = keys[k] << 1
    keys[k] = poppedBits[k] === 1 ? keys[k] | 1 : keys[k]

    registers[k] = bit === 1 ? (registers[k] >>> 1 | maxs[k]) : registers[k] >>> 1
    
}

export const calcGeffeKey = () => {
    for(let j = 1; j <= 8; j++) {
        // calc poppedBits for geffeKey
        for(let i = 0; i <= 2; i++) {
            calcNextKey(i)
        }

        geffeKey = geffeKey << 1
        const resultBit = poppedBits.reduce((prev, next) => prev ^ next)
        geffeKey = resultBit & 1 === 1 ? geffeKey | 1 : geffeKey


    }
    
    return geffeKey
}