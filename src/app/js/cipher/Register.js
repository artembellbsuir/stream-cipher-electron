let register, key = 0, masks, maxPower, max

const initRegister = (initialState, powers) =>  {
    register = initialState
    maxPower = Math.max(...powers)
    max = 2 ** (maxPower - 1)
    masks = powers.map(power => 2 ** (maxPower - power))
}

const calcNextKey = () => {
    for(let i = 1; i <= 8; i++) {
        key = key << 1
        key = register & 1 === 1 ? key | 1 : key

        register = masks
            .map(int => (register & int) === int)
            .reduce((prev, next) => prev ^ next) === 1 ? (register >>> 1 | max) : register >>> 1
    }

    return key
}

export {initRegister, calcNextKey}