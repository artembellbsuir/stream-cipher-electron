export default class Reg {
    constructor(register, powers = [3, 25]) {
        this.registerBytes = 4//Math.ceil(Math.max(powers) / 8)
        // 33554431 - max
        // this.register = new Uint32Array([23154121])//new ArrayBuffer(this.registerBytes)

        this.register = new Uint32Array([register]);

        this.powers = powers
        this.key = new Uint8Array([0]);
        this.masks = new Uint32Array([1, 4194304, 16777216])
    }

    shiftKey(bit) {
        this.key[0] = this.key[0] << 1
        this.key[0] = bit ? this.key[0] | 1 : this.key[0]
    }

    shift() {
        const x24 = (this.register[0] & this.masks[0]) === this.masks[0] 
        const x2 = (this.register[0] & this.masks[1]) === this.masks[1]

        // console.log(this.register[0].toString(2));
        const poppedBit = (this.register[0] & 1) === 1;
        this.shiftKey(poppedBit)

        this.register[0] = this.register[0] >>> 1

        const bit = x24 === x2 ? 0 : 1

        const prev = this.register[0]
        this.register[0] = bit ? (this.register[0] | this.masks[2]) : this.register[0]

        
    }
}