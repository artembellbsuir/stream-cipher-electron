import { hot } from "react-hot-loader/root";
import React from 'react';
import '../../styles/App.css';
import { remote } from 'electron';
const fs = require('fs')

import {
	Container,
	Row,
	Col,
	Form,
	Button,
	InputGroup,
	FormControl
} from 'react-bootstrap';

import {initRegister, calcNextKey} from '../cipher/Register'
import {initGeffeRegisters, calcGeffeKey} from '../cipher/Geffe'

class App extends React.Component {
	constructor() {
		super();

		this.state = {
			register: '1'.repeat(26),
			plaintext: '',
			ciphertext: '',
			inputFilePath: '',
			outputFilePath: '',

			lfsr1: '1100000000000000000000110000000000000110000000000000010000000000',
			lfsr2: '0001000000000000000000000000000000000000011111000001111100000111',
			lfsr3: '0001000000000000000000000000000000000000011111000001111100000111',
		}
	}
	
	async handleEncode(e) {
		initRegister(parseInt(this.state.register, 2), [26, 8, 7, 1])
		
		const arr = this.state.buffer
		const copy = arr.map(array => array.slice());

		const getResult = () => {
			return new Promise(resolve => {
				const result = copy.map(array => {
					return array.map(byte => {
						return byte ^ calcNextKey()
					})
				})
				console.log(result)

				let ciphertext = '', i = 0
				while(i < 63 && i + 1 !== result[0].length) {
					let s = result[0][i++].toString(2)
					ciphertext += this.upToByte(s)
				}

				this.setState({cipherArr: result, ciphertext})
				resolve(result)
			})
		}

		const promise = getResult()
	}

	upToByte(string) {
		return '0'.repeat(8 - string.length) + string
	}

	handleFileChoose() {
		let result = remote.dialog.showOpenDialogSync({ properties: ['openFile'] })  
		let file = result ? result[0] : ''

		this.setState({inputFilePath: file})

		try {
			let readBuffer = [],  index = 0
			const readStream = fs.createReadStream(file)

			readStream.on('data', chunk => {				
				readBuffer[index] = new Uint8Array(chunk);
				index++
			})
	
			readStream.on('end', () => {
				let plaintext = '', i = 0
				while(i < 63 && i + 1 !== readBuffer[0].length) {
					let s = readBuffer[0][i++].toString(2)
					plaintext += this.upToByte(s)
				}
	
				console.log(readBuffer);
				this.setState({buffer: readBuffer, plaintext})
			})	
		} catch (error) {
			console.log(error)
		}
	}

	handleOutputFileChoose() {
		let result = remote.dialog.showOpenDialogSync({ properties: ['openFile'] })  
		let file = result ? result[0] : ''  
		this.setState({outputFilePath: file})
	}

	handleSave() {
		const {cipherArr: encodedBytes, outputFilePath} = this.state 

		const writeStream = fs.createWriteStream(outputFilePath);

		for(let i = 0; i < encodedBytes.length; i++) {
			writeStream.write(encodedBytes[i]);
		}
	
		writeStream.on('finish', () => {
			writeStream.end();
		});
	}

	handleGeffe(e) {
		const {lfsr1, lfsr2, lfsr3} = this.state

		initGeffeRegisters(
			[parseInt(lfsr1, 2), parseInt(lfsr2, 2), parseInt(lfsr3, 2)],
			// [[3, 25], [13, 33], [5, 23]]// 25 33 23
			[[30, 16, 15, 1], [38, 6, 5, 1], [28, 3]]
		)

		const arr = this.state.buffer
		const copy = arr.map(array => array.slice());

		const getResult = () => {
			return new Promise(resolve => {
				const result = copy.map(array => {
					return array.map(byte => {
						const key = calcGeffeKey()
						console.log(byte, key, byte ^ key)
						return byte ^ key
					})
				})
				console.log(result)

				let ciphertext = '', i = 0
				while(i < 63 && i + 1 !== result[0].length) {
					let s = result[0][i++].toString(2)
					ciphertext += this.upToByte(s)
				}

				this.setState({cipherArr: result, ciphertext})
				resolve(result)
			})
		}

		const promise = getResult()
	}

	render() {
		return (
			<Container>
				<Row>
					<Col md={6}>
						<Form>
							<Form.Group controlId="inputTextArea">
								<Form.Label>Input text ({'<'}= 64 bytes)</Form.Label>
								<Form.Control defaultValue={this.state.plaintext} as="textarea" rows="12" />
							</Form.Group>
						</Form>
					</Col>
					<Col md={6}>
						<Form>							
							<Form.Group controlId="outputTextArea">
								<Form.Label>Ouput text ({'<'}= 64 bytes)</Form.Label>
								<Form.Control defaultValue={this.state.ciphertext} as="textarea" rows="12" />
							</Form.Group>
						</Form>
					</Col>
				</Row>
				
				<Row>
					<Col>
						<Form>
							<Form.Group controlId="keyFiles">
								<InputGroup className="mb-3">
									<InputGroup.Prepend>
										<Button 
											onClick={() => this.handleFileChoose()}
											variant="outline-primary">Choose input file</Button>
									</InputGroup.Prepend>
									<FormControl readOnly aria-describedby="basic-addon1" value={this.state.inputFilePath} />
								</InputGroup>
								<InputGroup className="mb-3">
									<InputGroup.Prepend>
										<Button
											onClick={() => this.handleOutputFileChoose()}
											variant="outline-primary">Choose output file</Button>
										<Button 
											onClick={() => this.handleSave()}
											disabled={this.state.outputFilePath == ''} 
											variant="outline-success">Save</Button>
									</InputGroup.Prepend>
									<FormControl readOnly aria-describedby="basic-addon1" value={this.state.outputFilePath} />
								</InputGroup>
							</Form.Group>							
						</Form>
					</Col>
				</Row>

				<Row>
					<Col>
						<Form>
							<Form.Group controlId="lonelyRegister">
								<Form.Label>LFSR initial state</Form.Label>
								<InputGroup className="mb-3">
									<Form.Control
										type="text"
										placeholder="LFSR"
										value={this.state.register}
										onChange={(e) => this.setState({register: e.target.value})}	
									/>
								</InputGroup>
								<Button 
									variant="success"
									onClick={(e) => this.handleEncode(e)}	
								>LFSR</Button>
							</Form.Group>
						</Form>
					</Col>
				</Row>

				<Row>
					<Col>
						<Form>
							<Form.Group controlId="geffeRegister">
								<Form.Label>Geffe initial state</Form.Label>
								<InputGroup className="mb-3">
									<Form.Control
										type="text"
										placeholder="LFSR (1)"
										value={this.state.lfsr1}
										onChange={(e) => this.setState({lfsr1: e.target.value})}	
									/>
									<Form.Control
										type="text"
										placeholder="LFSR (2)"
										value={this.state.lfsr2}
										onChange={(e) => this.setState({lfsr2: e.target.value})}	
									/>
									<Form.Control
										type="text"
										placeholder="LFSR (3)"
										value={this.state.lfsr3}
										onChange={(e) => this.setState({lfsr3: e.target.value})}	
									/>
								</InputGroup>

								<Button 
									variant="success"
									onClick={(e) => this.handleGeffe(e)}	
								>Geffe</Button>
							</Form.Group>
						</Form>
					</Col>
				</Row>

			</Container>
		)
	}
}

export default hot(App);
